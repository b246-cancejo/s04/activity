from abc import ABC, abstractclassmethod


class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass
    
    @abstractclassmethod
    def make_sound(self, sound):
        pass
    

class Dog(Animal):
    def __init__(self, name, breed, age) -> None:
        super().__init__()
        self.name = name
        self.breed = breed
        self.age = str(age)
        
    def call(self):
        print(f'Here {self.name}!')

    def eat(self, food):
        print(f'Eaten {food}')

    def make_sound(self):
        print('Bark! Woof! Arf!')
    
    def get_name(self):
        print(f"Name: {self.name}")
    
    def get_age(self):
        print(f"Age: {self.age}")

    def get_breed(self):
        print(f"Breed: {self.breed}")

    def set_name(self, name):
        self.name = name

    def set_breed(self, breed):
        self.breed = breed
        
    def set_age(self, age):
        self.age = age

class Cat(Animal):
    def __init__(self, name, breed, age) -> None:
        super().__init__()
        self.name = name
        self.breed = breed
        self.age = str(age)
        
    def call(self):
        print(f'{self.name}, come on!')

    def eat(self, food):
        print(f'Serve me {food}')

    def make_sound(self):
        print('Miaow! Nyaw! Nyaaa!')

    def get_name(self):
        print(f"Name: {self.name}")
    
    def get_age(self):
        print(f"Age: {self.age}")

    def get_breed(self):
        print(f"Breed: {self.breed}")

    def set_name(self, name):
        self.name = name

    def set_breed(self, breed):
        self.breed = breed
        
    def set_age(self, age):
        self.age = age

   

# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()


